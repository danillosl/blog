var bodyParser = require("body-parser"),
expressSanitizer = require("express-sanitizer")
    methodOverride = require("method-override"),
    mongoose = require("mongoose"),
    express = require("express"),
    app = express();

mongoose.connect("mongodb://localhost/blog", { useMongoClient: true });

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer());
app.use(methodOverride("_method"));

var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: { type: Date, default: Date.now }
}, { collection: "blogs" });

var Blog = mongoose.model("Blog", blogSchema);

app.get("/", function (req, res) {
    res.redirect("/blogs");
})


app.get("/blogs", function (req, res) {
    Blog.find({}, function (err, blogs) {
        if (err) {
            console.log(err);
        } else {
            res.render("index", { blogs: blogs });
        }
    })
})

app.get("/blogs/new", function (req, res) {
    res.render("new");
})

app.post("/blogs", function (req, res) {

    req.body.blog.body = req.sanitize(req.body.blog.body);

    Blog.create(req.body.blog, function (err, blog) {
        if (err) {
            res.redirect("/blogs/new");
        } else {
            res.redirect("/blogs");
        }
    });

})

app.get("/blogs/:id", function (req, res) {

    Blog.findById(req.params.id, function (err, blog) {
        if (err) {
            res.redirect("/blogs");
        } else {
            res.render("show", { blog: blog })
        }
    })
})

app.get("/blogs/:id/edit", function (req, res) {
    Blog.findById(req.params.id, function (err, blog) {
        if (err) {
            res.redirect("/blogs");
        } else {
            res.render("edit", { blog: blog })
        }
    })

})

app.put("/blogs/:id", function (req, res) {

    req.body.blog.body = req.sanitize(req.body.blog.body);
    
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, function (err, blog) {
        if (err) {
            res.redirect("/blogs");
        } else {
            res.redirect("/blogs/" + req.params.id);
        }
    });
})

app.delete("/blogs/:id", function (req, res) {
    Blog.findByIdAndRemove(req.params.id, function (err, blog) {
        if (err) {
            res.redirect("/blogs/" + req.params.id);
        } else {
            res.redirect("/blogs/");
        }
    });
})


app.listen("3000", function () {
    console.log("server listening on port 3000");
})